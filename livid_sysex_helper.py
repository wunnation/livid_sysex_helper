#!/usr/bin/env python
from library.globalEditor_f import *
import sys
import time

#----------- Initialize Command Line Argument Controllable Variables -----------------
print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)
command_line_arguments = sys.argv
quick_connect = False
#----------- Initialize Command Line Argument Controllable Variables -------------END-

#----------- Interpret Command Line Arguments ----------------------------------------
for loop in range(len(command_line_arguments)):
    if command_line_arguments[loop] == 'q':
        quick_connect = True
#----------- Interpret Command Line Arguments ------------------------------------END-

#------------------Main Program -------------------#
clearScreen()
printHeader("Livid Sysex Helper - Scanning Available MIDI Ports...")
pypm.Initialize()
MidiPorts = GetFullMidiList()
MidiInputs, MidiOutputs, MidiIONumbers = GetMidiLists() #Revision: Maybe Get Rid of this...
MidiPairs = GetMidiPairs(MidiInputs, MidiOutputs, MidiIONumbers)
#print MidiPairs
#print MidiInputs
#print MidiOutputs
del MidiInputs
del MidiOutputs
del MidiIONumbers
RecognizedMidiControllers = GetRecognizedMidiControllers(MidiPorts, MidiPairs)
#PrintAllMIDIDevices()
#print MidiPairs

quit = 0
channel = DEFAULT_SEND_CHANNEL
controller_timeout = 200
midiLatency_connecting = 25
#midiLatency_testing = 0
midiLatency_testing = 25 # !Revision: Windows
#midiLatency_testing = 0 # !Revision: Linux
#midiLatency_testing = 0 # !Revision: Mac

#!Revision: Seems to seg-fault when you switch controllers alot.
# - This has been solved in newer softwares that initialize the MIDI System inside a class.

while (not quit):	#--------Main Loop (1)--------
    controller_loop = 0
    option_number = 0
    #clearScreen()
    
    #------------Detect, Select, and Open A Midi Device-------------
    RecognizedMidiControllers = GetRecognizedMidiControllers_complete(controller_timeout,midiLatency_connecting)
    
    if quick_connect and len(RecognizedMidiControllers):    # A. quick_connect (Command Line Argument) -> Connects to First Recognized Controller
        printHeader("Recognized Controllers Found... QuickConnect...")
        cn = RecognizedMidiControllers[0][0]
        min = RecognizedMidiControllers[0][1]
        mon = RecognizedMidiControllers[0][2]
        MidiIn, MidiOut = OpenMidiIO(min,mon,midiLatency_testing)
    elif len(RecognizedMidiControllers):                    # B. Regular Connect -> Select between all recognized Controllers
        printHeader("Recognized Controllers Found...")
        option_number = 0
        while option_number < len(RecognizedMidiControllers):
            print option_number+1,".)", ControllerNames[RecognizedMidiControllers[option_number][0]]
            option_number +=1
        print option_number+1, ".) I want to select Midi-Ports Manually."
        selected_option = GetABYTE("> ",1,len(RecognizedMidiControllers)+2, options_enabled = 1)
        if selected_option == "error":
            MidiIn = None
            MidiOut = None
            pypm.Terminate()		#----Always Errors Upon Closing....
            print "Closing Ports and Exiting"
            time.sleep(.2)
            sys.exit()
        elif selected_option <= len(RecognizedMidiControllers):
            selected_option -= 1
            cn = RecognizedMidiControllers[selected_option][0]
            min = RecognizedMidiControllers[selected_option][1]
            mon = RecognizedMidiControllers[selected_option][2]
            MidiIn, MidiOut = OpenMidiIO(min,mon,midiLatency_testing)
        else:                                               # C. Manual Connect -> Identify Midi Input and Output Separately
            printHeader("Recognized Controller with Invalid Controller Number")
            cn = SelectControllerByName()
            if cn == "error":
                MidiIn = None
                MidiOut = None
                pypm.Terminate()		#----Always Errors Upon Closing....
                print "Closing Ports and Exiting"
                time.sleep(.2)
                sys.exit()
            else:
                min,MidiIn,mon,MidiOut = GuidedConnect(99, midiLatency_testing)
    else:                                                   # C2. Manual Connect
        printHeader("No Recognized Controllers Found...")
        cn = SelectControllerByName()
        if cn == "error":
            MidiIn = None
            MidiOut = None
            pypm.Terminate()		#----Always Errors Upon Closing....
            print "Closing Ports and Exiting"
            time.sleep(.2)
            sys.exit()
        else:
            min,MidiIn,mon,MidiOut = GuidedConnect(99, midiLatency_testing)
    #------------Detect, Select, and Open A Midi Device------end----
        
    while (not controller_loop):	#------Controller Loop (2)--------
        #clearScreen()
	#------------Header--------------
        if cn < len(ControllerNames):
            printHeader(ControllerNames[cn]+" Livid ID: ["+str(cn)+"]   Midi I,O Address ["+str(min)+","+str(mon)+"] - Select A Message/ Option")
        else:
            printHeader("Custom Controller"+" ["+str(cn)+","+str(min)+","+str(mon)+"] - Select A Message/ Option")
        #-----------Header----end-------                                                                                

        sxn = SelectMessageType()  # returns string 'error' if the user wants to quit
        #print "SXN: ", sxn
	#-----------Branch Based on Message Type---------
        if sxn == "error":
            MidiIn = None # ---- Ensure Created MIDI Ports are deleted
            MidiOut = None  #---- Ensure Created MIDI Ports are deleted
            pypm.Terminate() #---- Close MIDI Ports
            print 'Exiting Program...'
            sys.exit()      # get out of here
        elif sxn < len(SysExNames):					# Review: Define GuidedSysEx(cn, sxn)
            if cn < len(ControllerNames):
                #clearScreen()
                if cn < len(ControllerNames):
                    printHeader(ControllerNames[cn]+": "+SysExNames[sxn])
                else:
                    printHeader("CustomController: "+SysExNames[sxn])
                GuidedSysEx(MidiIn, MidiOut, cn, sxn)
            else:
                print "You Must have A Recognized Controller Number to use these Messages"
            temp_text = raw_input(" >").lower()
            if len(temp_text):
                if temp_text[0] == 'q': sxn = "error"	# Pause so the user can see the results (let them quit)
        elif sxn in range( len(SysExNames) , 128 ):		# Note/CC's
            CustomSysEx(cn, MidiOut, sxn)
        elif sxn in range( 128 , 128+4 ):		# Note/CC's
            message_number = sxn-128
            #print clearScreen()
            if cn < len(ControllerNames):                
                printHeader(ControllerNames[cn]+": "+MessageNames[message_number]+' - Channel: '+ str(channel)+' (of 15)')
            else:
                printHeader("Custom Controller: "+MessageNames[message_number]+' - Channel: '+ str(channel)+' (of 15)')
            GuidedNoteCC(message_number, channel, MidiOut)

        elif sxn in range(128+4, 128+len(MessageNames)):	# 
	    print "Unimplemented Functions..."
        elif sxn == 128 + len(MessageNames): #Revision: OptionNames
            channel = GetABYTE("Enter New Channel Number (" +  str(channel)+ " of 15)" + ": ")
        elif sxn == 128 + len(MessageNames) + 1:
	    CustomSysEx(cn, MidiOut)
        elif sxn == 128 + len(MessageNames) + 2:
	    SysExPreset(cn, MidiIn, MidiOut)
	#-----------Branch Based on Message Type-----end---

	#-----------What do we do next?-----------
        if (sxn == "error"):
            MidiIn = None
            MidiOut = None
            controller_loop = 1
            quit = 1
	#-----------What do we do next?-----end---
    #--------Controller Loop (2) End----
#--------Main Loop (1)---End--------

#--------Post Program Operations-------
time.sleep(1) # Let any pending operations complete
MidiIn = None
MidiOut = None
pypm.Terminate()		#----Always Errors Upon Closing....
#--------Post Program Operations---end-
#------------------Main Program -------------------#


