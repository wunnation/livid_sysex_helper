Livid Sysex Helper
Published by J.Moon on 1/30/15
Requirements: Python and Python-PortMidi

This application helps send templated sysex messages to Livid MIDI Controllers.
The program can also send text based MIDI Preset Files.
This program does not follow PEP-8 conventions, and is very rudimentary.
It is provided as a reference for script developers.


Keyboard Commands===========
Data Entry---
When sending sysex data, you can use a few shortcuts to make it quiker.

Name: Multiplication
Input: 12*8 
Response: Adds the number 12 to the sysex packet eight times.

Name: Ascending Sequence
Input: 1/12
Response: Adds the sequence [1,2,3,4,5,6,7,8,9,10,11,12] to the sysex packet.

Name: Ascending Sequence, Alternating (useful for Mapping Commands)
Input: 1/5,1
Response: Adds the sequence [1,1, 2,1, 3,1, 4,1, 5,1] to the sysex packet

Build Notes ================
Ohm64 and Block may not be recognized by the Startup Sequence, and must be selected manually.