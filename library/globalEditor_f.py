# Livid MIDI Controller Editor Functions for PyPortMidi
# July 21, 2011: accommodate for SysEx messages and preferred list formats
# see /usr/include/portmidi.h for the list of functions in C

import os
from random import randint
from math import ceil 	# used only for Dividing Program Options into Columns

from pypmExtensions_f import * # pypmExtensions
from fileOps_f import * # used for Preset Files
#------------------User Interface-------------------------#

#--------SelectControllerByName----------------------#
# Function: Prints List of Livid Controllers
#----------------------------------------------------#
def SelectControllerByName():
    #clearScreen()
    print LINE_BREAK
    print "Please Select the Controller that you wish to Configure"
    print LINE_BREAK
    for loop in range(0,len(ControllerNames)):
        print loop," - ", ControllerNames[loop]
    print "99 - <Other-Controller>"
    print LINE_BREAK
    n = GetABYTE("Enter Controller #:", options_enabled = 1)
    if (n < len(ControllerNames)):
        return int(n)
    else:
        return n
    print LINE_BREAK

#--------GetMIDIOutput----------------------#
# Function: Gets user selection for a Controller's PortMidi Hardware Port Output 
#----------------------------------------------------#
def GetMIDIOutput():
    midiOuts = []
    for loop in range(pypm.CountDevices()):
        interf,name,inp,outp,opened = pypm.GetDeviceInfo(loop)
        if (outp ==1):
           midiOuts.append(loop)
           print loop, name," ",
           print "(output) ",
           if (opened == 1): print "(opened)"
           else: print "(unopened)"
    n = 99 #error
    while not n in midiOuts:
        n = GetABYTE("Type Output Number:")
    return n


#--------GetMIDIInput----------------------#
# Function: Gets user selection for a controller's PortMidi Hardware Port Input
#----------------------------------------------------#
def GetMIDIInput():
    midiIns = []
    for loop in range(pypm.CountDevices()):
        interf,name,inp,outp,opened = pypm.GetDeviceInfo(loop)
        if (inp == 1):
            midiIns.append(loop)
            print loop, name," ",
            print "(input) ",
            if (opened == 1): print "(opened)"
            else: print "(unopened)"
    n = 99 #error
    while not n in midiIns:
        n = GetABYTE("Type Input Number:")
    return n

#--------Guided Connect----------------------#
# Function: Guides User into Connecting to a Controller by Controller Number
# (via Auto-detect or Manual Selection)
#----------------------------------------------------#
def GuidedConnect(controllerNumber, midi_latency = 100):
    #clearScreen()
    if (controllerNumber < len(ControllerNames)):
        min = SearchForControllerInputs(controllerNumber)
        mon = SearchForControllerOutputs(controllerNumber)
    else:
        min = 99
        mon = 99

    if min != 99 and mon !=99:
        SendSysEx(REQ_CONTROLLER_CONFIG)
    print LINE_BREAK
    if (min == 99):
        print "\nManual Select > Input:\n"
        min = GetMIDIInput()
    if (mon == 99):
        print "\nManual Select > Output:\n"
        mon = GetMIDIOutput()
    print LINE_BREAK
    return (min, pypm.Input(min), mon, pypm.Output(mon,midi_latency))

#--------Guided Connect----------------------#
# Function: Guides User into Connecting to a Controller by Controller Number
# (via Auto-detect or Manual Selection)
#----------------------------------------------------#
def QuickConnect_tx():
    #clearScreen()
    printHeader("Select a Port to send Messages to...")
    mon = GetMIDIOutput()
    print LINE_BREAK
    return (mon, pypm.Output(mon,100))


#--------SelectMessageType----------------------#
# Function: Prints a List of Messages/ Program Options, and forces the User to choose one.
#----------------------------------------------------#
def SelectMessageType():
    print"------------------------------------------------------------------------------------"
    print"SystemExclusive Messages				|	Other Midi Messages"
    print"------------------------------------------------------------------------------------"
#    for loop in range(len(SysExNames)):
    rows = int(ceil(float(len(SysExNames))/2))
    for loop in range(rows):
        col2_index = loop+rows
        print loop, " - ", SysExNames[loop],
        if col2_index < len(SysExNames): 
            print "		", col2_index, " - ", SysExNames[col2_index],
        if loop < len(MessageNames):
            #print "		",loop+len(SysExNames), " - ", MessageNames[loop],	#revision Text Organization  
            print "		",loop+128, " - ", MessageNames[loop],	#revision Text Organization  
        elif loop < len(MessageNames)+len(OptionNames):
            #print "		",loop+len(SysExNames), " - ", OptionNames[loop-len(MessageNames)],	#revision Text Organization  
            print "		",loop+128, " - ", OptionNames[loop-len(MessageNames)],	#revision Text Organization  
        print ' '
    return GetABYTE("Type Message Number:",0,128+len(MessageNames)+len(OptionNames), 1)

#--------GuidedNoteCC----------------------#
# Function: Guides User into Sending a Note or Control Change Message
#----------------------------------------------------#
def GuidedNoteCC(message, channel, MidiOut):
    c = MessageCodes[message]|channel
    n = 0
    v = 0
    finished = 0
    print "Enter Message (or vel/ q-to-quit) [message ,c ,n, v]\n"
    while not finished:
        print "Last Message: " + str(message) + " " + str(channel) + " " + str(n) + " " + str(v)
        print MessageNames[message], "		Channel ", channel
        mcnv = raw_input(" >")
        if (mcnv == 'q' or mcnv == 'Q'):
            finished = 1
            print ' '
        else:
            mcnv = mcnv.replace(',', ' ')
            mcnv = mcnv.split()
            for loop in range(len(mcnv)):
                try:
                    mcnv[loop] = int(mcnv[loop])
                except:
                    break
            index = len(mcnv)-1 # kinda redundant
            count = 0
            while index >= 0:
                if count == 0:
                    #print 'velocity'
                    if mcnv[index] >= 0 and mcnv[index] < 128:
                        v = mcnv[index]
                elif count == 1:
                    #print 'number'
                    if mcnv[index] >= 0 and mcnv[index] < 128:
                        n = mcnv[index]
                elif count == 2:
                    #print 'channel'
                    if mcnv[index] >= 0 and mcnv[index] < 16:
                        channel = mcnv[index]
                elif count == 3:
                    #print 'message ' + str(mcnv)
                    if mcnv[index] < 128 and mcnv[index] >= 0:
                        message = mcnv[index]
                index-=1
                count+=1
            c = MessageCodes[message]|channel
            print "Sending: " + str([c,n,v])
            print ' '
            MidiOut.WriteShort(c,n,v)
            #finished = 1   # We have a valid message

#--------GuidedSysEx----------------------#
# Function: Parses a User's Input and Returns the Resulting Message
# - loop is the current dataByte position
# - dby is the number of databytes in the current message
# - '*', ',' and '/' are special characters
#----------------------------------------------------#
def GetSysEx_DataInput(my_message, loop, dby):
    valid_input = 0
    all_done = 0
    while not valid_input:
        new_byte=raw_input("Enter Byte %d:"%loop)
        if len(new_byte):
            this_byte,that_byte,other_byte,mType = ParseSysEx_DataInput(new_byte)
            if (mType == SX_MULT):                          # Multi-Message Operator Detected: Look for special Characters 
                valid_input = 1
                i = 0					# InterpretSxMult()
                while i < other_byte and loop < dby:
                     my_message.append(this_byte & 0x7F)
                     loop+=1
                     i+=1
                     if that_byte != GLOBALEDITOR_EMPTY_DATAINPUT:
                         my_message.append(that_byte & 0x7F)
                         loop+=1                 # Do not increment 'i' here 
            elif mType == SX_SERIES:
                valid_input = 1
                this_byte = this_byte
                that_byte = that_byte              # !Revision: Allow Decrementing Too
                while this_byte < that_byte+1 and loop < dby:
                    my_message.append(this_byte & 0x7F)
                    loop+=1
                    this_byte+=1
                    if other_byte != GLOBALEDITOR_EMPTY_DATAINPUT:
                        my_message.append(other_byte & 0x7F)
                        loop+=1
            elif mType == SX_SINGLE:
                valid_input = 1
                my_message.append(this_byte)
                loop+=1
            elif mType == GLOBALEDITOR_ERR_DATAINPUT:
                print "Bad Syntax: try again..."
        else:
            print ("No Input.  Filling Remaining DataBytes with 0's")
            my_message.append(0)
            loop+=1
            valid_input = 1
            all_done = 1
    #------------- GetSysExData -----------------------------
    return loop, my_message, all_done

#--------GuidedSysEx----------------------#
# Function: Guides User into Sending a Livid Templated SysEx Message
#----------------------------------------------------#
def GuidedSysEx(MidiIn, MidiOut, cn, sxn):
    my_message = [0xF0, 0x00, 0x01, 0x61]
    my_message.append(cn) #!error cn
    my_message.append(sxn)
    ds  = SysExDataSource[sxn]
    SysExVerifyEnabled = 1
    if (ds & 0x80):
        ds &=0x7F                                   # Strip the Flag
        print "Data Source: ", DataSourceNames[ds],
        dby = ControllerProperties[ds*NumberOfControllers +cn ]
    else:
        dby = ds
    print "DataBytes: ", dby

    all_done = 0
    loop = 0

    #-----Formulate Message----------------------------
    while loop < dby:
        if all_done == 1:
            my_message.append(0)
            loop += 1
        else:
            loop, my_message, all_done = GetSysEx_DataInput(my_message, loop, dby)
 
    my_message.append(0xF7)
    #-----Formulate Message--------------end-----------
    
    #-----Send Message and Get Acknowledgement---------
    timed_out = 1
    timeout = 0
    waiting = 1
    while timed_out and waiting:
        if (cn < len(ControllerNames)):
            #clearScreen()
            if cn < len(ControllerNames):
                printHeader(ControllerNames[cn]+" - "+SysExNames[sxn]+": Writing Message...")
            else:
                printHeader("Custom Controller - "+SysExNames[sxn]+": Writing Message...")            
        print my_message
        MidiOut.WriteSysEx(pypm.Time(),my_message)
        if (SysExVerifyEnabled):
            if sxn in LividRequests:
                rx_message, timeout = GetSysExReq(MidiIn, cn)  #!Revision: Make it check the LED Numbers that are sent back for accuracy
            else:
                timeout = GetSysExAck(MidiIn, cn)
            if not timeout:				#!Review: Clumsy
                timed_out = 0
            if timed_out:
                retry = raw_input(str(waiting)+ ". Ack Not Received. Resend?")
                if retry != 'y':
                    waiting = 0
                    timed_out = 2
                else:
                    waiting+=1
    	else:
    	    waiting = 0
    #-----Send Message and Get Acknowledgement-----end--

#--------GuidedSysEx--------------End------#

    
#--------CustomSysEx----------------------#
# Function: Allows the user to send a Custom SysEx Message
#----------------------------------------------------#
def CustomSysEx(cn, MidiOut, message_number = 0xFF):
    MESSAGE_SIZE_LIMIT = 1000
    my_message = [0xF0, 0x00, 0x01, 0x61]
    if cn < len(ControllerNames):
        my_message.append(cn)
    if message_number < 128:
        my_message.append(message_number)
    
    all_done = 0
    loop = 0
    while not all_done:
        #clearScreen()
        if cn < len(ControllerNames):
            printHeader(ControllerNames[cn])
        else:
            printHeader("Custom Controller")    
        
        print "Your Message So Far: ", my_message, "..."
        new_byte=raw_input("Enter Byte %d:"%loop)
        if len(new_byte):
            if new_byte.lower() == 'q':
                return 0 # do not send message
            else:    
                this_byte,that_byte,other_byte,mType = ParseSysEx_DataInput(new_byte)
                if (mType == SX_MULT):                          # Multi-Message Operator Detected: Look for special Characters 
                    i = 0
                    while i < int(other_byte) and loop < MESSAGE_SIZE_LIMIT:
                         my_message.append(int(this_byte) & 0x7F)
                         loop+=1
                         i+=1
                         if len(str(that_byte)):		#!revision: Call to str is a quick-patch
                             my_message.append(int(that_byte) & 0x7F)
                             loop+=1                 # Do not increment 'i' here 
                elif mType == SX_SERIES:
                    this_byte = int(this_byte)
                    that_byte = int(that_byte)
                    while this_byte < that_byte+1 and loop < MESSAGE_SIZE_LIMIT:
                        my_message.append(this_byte & 0x7F)
                        loop+=1
                        this_byte+=1
                        if len(other_byte):
                            my_message.append(int(other_byte) & 0x7F)
                            loop+=1
                else:
                    my_message.append(int(new_byte))
                    loop+=1
        else:
            print ("No Input.  Truncating Message with 0xF7...")
            my_message.append(0xF7)
            loop+=1                
            all_done = 1
    print my_message
    MidiOut.WriteSysEx(pypm.Time(),my_message)
#--------CustomSysEx----------------End---#

#--------GetNumberOfMessages----------------------
# Receives: A File to Read
# Returns: Number of SysExMessages in that File
#-------------------------------------------------
def GetNumberOfMessages(readFile):
    messages = 0
    buffer1 = readFile.readline()
    while len(buffer1):
        message = buffer1.split()
        #message = buffer1.split(',')
        if len(message) > 2:
            try: 
                temp_var = int(message[0])
                if int(message[0]) == 240: 
                    messages+=1
            except:
                print "Byte would not convert to int, when split by ' '"
        else:
            print "SysEx was Too Short: "+str(message)
        buffer1 = readFile.readline()
    readFile.seek(0)
    return messages
#--------GetNumberOfMessages------------end-------

#--------TranslateTextToSysEx----------------------
# Receives: A SysEx Message as a Single String
# Returns: A SysEx Message as a List of Bytes
#-------------------------------------------------
def TranslateTextToSysEx(raw_message):
    message = []
    count = 0
    buff = 0
    #buffer = raw_message.split(',')
    buffer = raw_message.split()
    while not buff == 247:
        buff = int(buffer[count])
        message.append(buff)
        count += 1
    print message 
    return message
#--------TranslateTextToSysEx------------end-------

#--------SysExPreset----------------------
# Receives: Controller Number, MidiInput Port, MidiOutput Port
# Returns:  < nothing >
# Does: Sends a Bunch of Messages to your controller in Succession
#       Receives Acknowledgement Responses
#-------------------------------------------------
def SysExPreset(cn, MidiIn, MidiOut):
    messages = 0
    EnableTabAutoComplete();
    file_selected = 0
    while not file_selected:
        filename = raw_input ("Preset File:>")
        if filename == 'q' or filename == 'Q':
           return
        print "Opening the File..."
        try:
            readFile = open(filename, 'r')
            file_selected = 1
        except:
            print "Invalid File Name. Please Try again."
            file_selected = 0

    messages = GetNumberOfMessages(readFile)
    #---------------All Messages Loop---------------------#
    for loop in range(messages):
        split_message = []
        raw_message = '\n'
        while len(split_message) < 3 and len(raw_message):		#!revision: MIDIMessageSize_Minimum/ Make Sure this syncs up with GetNumberOfMessages
            raw_message = readFile.readline()
            if not '#' in raw_message:
                split_message = raw_message.split()
            else:
                print 'comment: ' + raw_message
                split_message = []
        #raw_input("Press Enter to Send> ")        
        #-----------Retry Loop----------------------------#
        timeout = 0xFF
        cntr = 0
        #while timeout and cntr < 5:               #timer1_speed in range(TIMER1_MIN, TIMER1_MAX):
        while timeout and cntr < 1:               #timer1_speed in range(TIMER1_MIN, TIMER1_MAX):
            print raw_message
            message = TranslateTextToSysEx(raw_message)
            MidiOut.WriteSysEx(pypm.Time(),message)
            #---Get Ack(MidiIn, timeout in ms)------return--message or TimedOut-----------#
            if cn < NumberOfControllers:
		if len(message) > 5:
                    messageType = message[5]
                else:
                    messageType = 0xFF		# Unknown Message

                print "Sent Configure Request.  Awaiting Response..."
                if messageType in LividRequests:
                    rx_message, timeout = GetSysExReq(MidiIn, cn)  #!Revision: Make it check the LED Numbers that are sent back for accuracy
                else:
                    timeout = GetSysExAck(MidiIn, cn)

                if timeout:
                    cntr += 1
                    print "Try: ", cntr,  " No Ack Received... Repeating"
                    if cntr > 0:
                        ta = raw_input (str(cntr)+" Failures: Try again (y/n)?")
                        if ta == 'y' or ta == 'Y':
                            cntr = 0
                        else:
                            readFile.close()
                            return;
            else:
                print "Sent Configure Request. Unknown Controller Cannot Receive Response"
                timeout = 0 
    readFile.close()
            #---Get Ack-------------------------#
        #-----------Retry Loop----------------------------#

    #---------------All Messages Loop---------------------#

#--------SysExPreset----------------end---
#------------------User Interface-------------------------#


#-----------------------------------------#
#------------------Input Processing-----------------------#
def ParseSysEx_DataInput(new_byte):		# revision: 2-sided (each side can have a * or / operator)
    this_byte_int, that_byte_int, other_byte_int = 0,0,0

    #--------------------------- Split Message By Operators -----------------------------------
    if ('*' in new_byte):		# revision: Ranges other than counting up by 1
        mType = SX_MULT
        this_byte, other_byte = new_byte.split('*')
        if (',' in this_byte):
            this_byte, that_byte = this_byte.split(',')
        else:
            that_byte = ''				                 # empty String
    elif ('/' in new_byte):
        mType = SX_SERIES
        this_byte, that_byte = new_byte.split('/')
        if (',' in that_byte):
            that_byte, other_byte = that_byte.split(',')
        else:
        	other_byte = ''			                       # empty String
    else:
        mType = SX_SINGLE
        this_byte,that_byte,other_byte = new_byte, '', ''
    #--------------------------- Split Message By Operators -------------------------_end_-----


    #--------------------------- Determine if is Acceptable Data -----------------------------------
    if not len(this_byte):
        this_byte_int = GLOBALEDITOR_EMPTY_DATAINPUT
    else:
        try: this_byte_int = int(this_byte)
        except: mType = GLOBALEDITOR_ERR_DATAINPUT #this_byte_int = GLOBALEDITOR_EMPTY_DATAINPUT
    if not len(that_byte):
        that_byte_int = GLOBALEDITOR_EMPTY_DATAINPUT
    else:
        try: that_byte_int = int(that_byte)
        except: mType = GLOBALEDITOR_ERR_DATAINPUT #that_byte_int = GLOBALEDITOR_EMPTY_DATAINPUT
    if not len(other_byte):
        other_byte_int = GLOBALEDITOR_EMPTY_DATAINPUT
    else:
        try: other_byte_int = int(other_byte)
        except: mType = GLOBALEDITOR_ERR_DATAINPUT #other_byte_int = GLOBALEDITOR_EMPTY_DATAINPUT
    #--------------------------- Determine if is Acceptable Data ---------------------_end_---------



    return (this_byte_int,that_byte_int,other_byte_int, mType)
#------------------Input Processing-----------------------#

#------------------SysEx Data Collecting-----------------------#
#def GetAllSysExData(MidiIn, MidiOut, cn):
#    before_reset_message_table = []
#    after_reset_message_table = []
#    for loop in range(127):
#        response_table.append([])
#        respons_table
#        after_reset_message_table.append([])


# ------------ Write Data to All SysEx---------------------------#
# Receives: data_value (MIDI Compatible 0-127)
# Note: data_value over MIDI Max (127) will result in random data being written instead
# Returns: List of Boolean Values  (1-per SysEx Message)
# - True, if Acknowledgement of Send Received/ False, Otherwise
# ----------------------------------------------------------------
def WriteDataToAllSysEx(MidiIn, MidiOut, cn, data_value): 
    response_table = []
    if data_value > MIDI_MAX_VALUE:
        random_numbers = True
    else:
        random_numbers = False
    for sysex_number in range(len(SysExNames)):
        response_table.append(False)

    for sysex_number in range(len(SysExNames)):
        #----- Get Message Properties For this controller------------
        data_source  = SysExDataSource[sysex_number]
        if (data_source & 0x80):
            data_source &=0x7F                                   # Strip the Flag
            print "Data Source: ", DataSourceNames[data_source],
            data_bytes = ControllerProperties[data_source*NumberOfControllers +cn ]
        else:
            data_bytes = data_source
        print "DataBytes: ", data_bytes
        #----- Get Message Properties For this controller--------end-

        if not data_bytes or sysex_number == LIVID_CHANGE_BANK:   # Skip Messages that have no data bytes
            continue         # They are usually Save Messages

        #----- Prepare Write Message ->  -----#
        #example message = [240, 0, 1, 97, 11, 7, 4, 0, 0, 247]
        message = [0xF0, 0x00,0x01, 0x61]           # Livid SysEx Header
        message.append(cn)                          # Controller Number
        message += [sysex_number] # Command Number
        for current_data_byte in range(data_bytes):
            if random_numbers:
                data_value = randint(0,127)
            message.append(data_value)                          # !error: appending 0's for now
        message.append(MIDI_SYSEX_END)                    # and every message accepts 0's
        #----- Prepare Write Message -> --end-#

        #---Prepare Expected Response Message----------#
        #example response =  [240, 0, 1, 97, 11, 4, 0, 0, 0, 0, 0, 0, 0, 0, 247]
        expected_response = [0xF0, 0x00, 0x01, 0x61]
        expected_response.append(cn)
        expected_response.append(LIVID_SYSEX_ACK)
        #---Prepare Expected Response Message-------end-#
        real_response = SendMessageReturnResponse(MidiIn, MidiOut, message, expected_response, timeout=300)
        if len(real_response):
            response_table[sysex_number] = True
    return response_table
# ------------ Write Data to All SysEx---------------------end---#

# ------------ Read All SysEx Messages---------------------------#
# Receives: MidiIn, MidiOut, Controller Number
# Returns: List of Response Messages (each of which is a list)
# - List of Response Messages is indexed by sysex_number
# - True, if Acknowledgement of Send Received/ False, Otherwise
# ----------------------------------------------------------------
def ReadAllSysEx(MidiIn, MidiOut, cn): 
    response_table = []
    for sysex_number in range(len(SysExNames)):
        response_table.append([])    
    
    for sysex_number in range(len(SysExNames)):
        #output_string = "Scanning: " + 'f:str(pings) + " Errors: " + str(errors) + " Last Error:" + str(last_error_code)
        output_string = "Reading: " + '{:3d}'.format(sysex_number) + ' of ' + '{:3d}'.format(len(SysExNames)-1)
        temp_var = os.write(1,'\r'+ output_string)  # Carriage Return Code: CR: Repeat Text on a single line of output

        #----- Prepare Data Request Message  -----#
        #example message = [240, 0, 1, 97, 11, 7, 4, 0, 0, 247]
        request_message = [0xF0, 0x00,0x01, 0x61]           # Livid SysEx Header
        request_message.append(cn)                          # Controller Number
        request_message += [LIVID_SYSEX_REQUEST, sysex_number] # Command Number
        request_message += [0, 0]                              # Data: (we use 0's in this case because we don't care)
        request_message.append(MIDI_SYSEX_END)                    # and every message accepts 0's
        #----- Prepare Data Request Message  --end-#

        #---Prepare Expected Response Message----------#
        #example response =  [240, 0, 1, 97, 11, 4, 0, 0, 0, 0, 0, 0, 0, 0, 247]
        expected_response = [0xF0, 0x00, 0x01, 0x61]
        expected_response.append(cn)
        expected_response.append(sysex_number)
        #---Prepare Expected Response request_message-------end-#
        response_table[sysex_number] = SendMessageReturnResponse(MidiIn, MidiOut, request_message, expected_response, timeout=300)
        #print str(sysex_number) + ' ' + SysExNames[sysex_number] + '. ' + str(response_table[sysex_number]) 
#        request_response = SendMessageReturnResponse(MidiIn, MidiOut, request_message, expected_response, timeout=300)
#        if not len(request_response):
#            print 'Error Reequest Message: This should not be possible'
#            continue
    return response_table
# ------------ Read All SysEx Messages--------------------end----#


# ------------ Save Controller Settings ---------------------------#
# Receives: MidiIn, MidiOut, Controller Number
# Sends: The Save Settings SysEx to a Connected Controller
# Returns: True, if the save was acknowledged
# ----------------------------------------------------------------
def SaveControllerSettings(MidiIn, MidiOut, cn):

    #----- Get Message Properties For this controller------------
    #----- Get Message Properties For this controller--------end-

    #----- Prepare Write Message ->  -----#
    #example message = [240, 0, 1, 97, 11, 2, 247]
    #sysex_number = SysExNames.index(LIVID_SAVE_SETTINGS)
    message = [0xF0, 0x00,0x01, 0x61]           # Livid SysEx Header
    message.append(cn)                          # Controller Number

    if cn in [4,5,6,7,8]:		# Legacy: Controllers with 4 Banks with individual mappings
        message.append(LIVID_SAVE_ALLBANKS) # Command Number

    else: #[0,1,2,3, 9,10,11,12,13]
        message.append(LIVID_SAVE_SETTINGS) # Command Number

    message.append(MIDI_SYSEX_END)                    # and every message accepts 0's
    #----- Prepare Write Message -> --end-#

    #---Prepare Expected Response Message----------#
    #example response =  [240, 0, 1, 97, 11, 127, 247]
    expected_response = [0xF0, 0x00, 0x01, 0x61]
    expected_response.append(cn)
    expected_response.append(LIVID_SYSEX_ACK)
    #---Prepare Expected Response Message-------end-#

    real_response = SendMessageReturnResponse(MidiIn, MidiOut, message, expected_response, timeout=300)
    if len(real_response):
        return True 
    else:
        return False
# ------------ Save Controller Settings ---------------------end---#

# ------------ Reset Controller Settings ---------------------------#
# Receives: MidiIn, MidiOut, Controller Number
# Sends: The Save Settings SysEx to a Connected Controller
# Returns: True, if the save was acknowledged
# ----------------------------------------------------------------
def ResetControllerSettings(MidiIn, MidiOut, cn):

    #----- Get Message Properties For this controller------------
    #----- Get Message Properties For this controller--------end-

    #----- Prepare Write Message ->  -----#
    #example message = [240, 0, 1, 97, 11, 2, 247]
    #sysex_number = LIVID_FACTORY_RESET
    message = [0xF0, 0x00,0x01, 0x61]           # Livid SysEx Header
    message.append(cn)                          # Controller Number
    message.append(LIVID_FACTORY_RESET) # Command Number
    message.append(MIDI_SYSEX_END)                    # and every message accepts 0's
    #----- Prepare Write Message -> --end-#

    #---Prepare Expected Response Message----------#
    #example response =  [240, 0, 1, 97, 11, 127, 247]
    expected_response = [0xF0, 0x00, 0x01, 0x61]
    expected_response.append(cn)
    expected_response.append(LIVID_SYSEX_ACK)
    #---Prepare Expected Response Message-------end-#

    real_response = SendMessageReturnResponse(MidiIn, MidiOut, message, expected_response, timeout=300)
    if len(real_response):
        return True      
    else:
        return False
# ------------ Reset Controller Settings ---------------------end---#


# ------------ Prepare SysEx Message ------------------------------#
# -------Returns Ghost Message (with 0's for all dataBytes) -------#
# -----------------------------------------------------------------#
def PrepareSysExMessage(cn, sysex_number):

    #----- Get Message Properties For this controller------------
    data_source  = SysExDataSource[sysex_number]
    if (data_source & 0x80):
        data_source &=0x7F                                   # Strip the Flag
        #print "Data Source: ", DataSourceNames[data_source],
        data_bytes = ControllerProperties[data_source*NumberOfControllers +cn ]
    else:
        data_bytes = data_source
    #print "DataBytes: ", data_bytes
    #----- Get Message Properties For this controller--------end-
    message = [LIVID_SYSEX_HEADER]
    message.append(cn)
    message.append(sysex_number)
    for current_data_byte in range(data_bytes):
        message.append(0)                          # !error: appending 0's for now
    message.append(MIDI_SYSEX_END)                    # and every message accepts 0's
    return message


# ------------ Prepare SysEx Message -------------------------end--#


#-------------  Save Controller Settings -------------end--------#


