#-------------------------------------------------------------------
# File:        globalEditor_h.py
# Description: Contains Constants associated with Controller
#              configurations associated with the globalEditor.py
#-------------------------------------------------------------------

#--------------ControllerNames: Indexed by ControllerNumber--------
ControllerNames 	= ("ALL", 	"Brain", 	"Ohm64", 	"block", 	"Code", 	"StudioBlade", 	"MCSM", 	"OhmRGB",	"Cntrl_r",	"Brain2",	"Enlighten",	"Alias8", 	"Base8",	"BrainJr", 	"BrainCV",	"Wing", 	"Mixxr", 	"Basev2")
# Note: BrainCV -> May need updates to LEDGroups

NumberOfControllers	= len(ControllerNames)

#--------------ControllerProperties: Indexed By (ControllerNumber and DataBytes)-----
#00-2*iLEDColumns		
ControllerProperties	= (0,		14,		12,		10,		8,		14,		0,		42,		30,		32,		16,		8,		32,		8,		12,		0,		8,		32,
#81-Buttons*2		= (
			   0,		248,		176,		144,		90,		128,		0,		176,		120,		128,		128,		32,		32,		32,		32,		0,		32,		32,
#82-Buttons2		= (
			   0,		0,		0,		0,		0,		57,		0,		0,		0,		128,		128,		0,		0,		0,		0,		0,		0,		0,
#83-Pots*2		= (
			   0,		128,		50,		20,		0,		52,		0,		50,		64,		128,		24,		96,		64,		32,		32,		8,		96,		64,
#84-ExpansionPots*2	= (
			   0,		0,		0,		20,		0,		0,		0,		20,		20,		0,		0,		0,		0,		0,		0,		0,		0,		0,
#85-Banks		= (
			   0,		0,		4,		0,		4,		4,		0,		4,		4,		0,		0,		15,		8,		1,		1,		0,		15,		8,
#86-Colors		= (
			   0,		0,		0,		0,		0,		0,		0,		8,		8,		8,		0,		8,		8,		8,		8,		0,		8,		8,
#87-Encoders*2		= (
			   0,		17,		0,		0,		64,		56,		0,		0,		24,		128,		24,		8,		36,		16,		16,		0,		8,		36,
#88-EncoderBitBytes*2	= (
			   0,		0,		0,		0,		8,		8,		0,		0,		4,		16,		4,		2,		0,		2,		2,		0,		2,		0,
#89-Notes/CCs		= (
			   128,		128,		128,		128,		128,		128,		128,		128,		128,		128,		128,		128,		128,		128,		128,		0,		128,		128,
#8A-Banks*2*iLEDCols	= (
			   0,		0,		0,		16,		0,		0,		0,		168,		120,		0,		16,		120,		224,		8,		12,		0,		120,		224,
#8B-Encoders		= (
			   0,		0,		0,		0,		32,		28,		0,		0,		12,		64,		12,		4,		0,		8,		8,		0,		4,		0,
#8C-EncodersAllBanks	= (
			   0,		0,		0,		0,		128,		112,		0,		0,		48,		64,		12,		60,		0,		8,		8,		0,		60,		0,
#8D-2*AnalogBitBytes	= (
			   0,		16,		8,		8,		0,		8,		0,		12,		12,		16,		4,		16,		8,		4,		4,		0,		16,		8,
#8E-AnalogsWithExp	= (
			   0,		64,		25,		20,		0,		26,		0,		35,		42,		64,		12,		41,		32,		16,		16,		0,		41,		32,
#8F-RealButtonColumns*2
			   0,		32,		22,		9,		12,		46,		0,		22,		22,		32,		32,		6,		4,		4,		8,		0,		6,		4,
#90-LEDGroups
			   0,		0,		0,		0,		32,		27,		0,		22,		12,		64,		0,		24,		11,		16,		16,		0,		24,		9,#Base: 9strips+2-optionstrips
#91-EncoderSpeeds
			   0,		0,		0,		0,		2,		2,		0,		0,		2,		2,		2,		30,		0,		2,		2,		0,		30,		0,
#92-CapFaders
			   0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		9,		0,		0,		0,		0,		9,
#93-RGB Strips
			   0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		9,		0,		0,		0,		0,		9,
#94-CapButtons*2
			   0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		16,		0,		0,		0,		0,		16,
#95-LEDGroups/2		  
			   0,		0,		0,		0,		16,		14,		0,		11,		6,		32,		0,		12,		6,		8,		8,		0,		12,		6,
#96-7Segment Displays
			   0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		2,		2,		0,		0,		0,		2,		2,
#97-CapFaders*2		  
			   0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		18,		0,		0,		0,		0,		18,
                          )

#----------SysExNames: Indexed By: SysEx Number------------------------------------------------- 
SysExNames 		= (	
				"ButtonDIY(Deprc)", "Slider(Deprc)", 	"SaveSettings", "ReadSettings", "LEDsCommand", 	"RecallSettings", "FactoryReset", 	"RequestInfo", 
			   	"UserSettings", 	"LEDMapping", 	"AnalogMaps", 	"ButtonMaps",	"Channel(Global)", "MIDI Output", "Erase LEDs", 	"xFaderFlip", 
			   	"EncoderMaps",		"EncoderMode",	"AnalogChannels","ButtonChannels","EncoderChannels","LEDChannels", "BankChannel",	"BankChannels", 
				"SaveBank", 		"SaveBanks", 	"CurrentBank",	"ButtonMap2",	"RestoreCurrBank", "EncoderRingMode","EncoderScaling","EncoderRingLEDs",
				"RingControl", 		"AnalogMaps2", 	"ColorMapping",	"LEDMapNotes",	"LEDMapCCs" , "LEDsAllBanks", "EncoderValues",		"EncoderValuesAllBanks",
				"EncodersEnableFlags",	"AnalogMode",	"LastLEDColumn","EncoderFlip" ,	"AnalogFlip",	"AnalogDisableFlags","LEDGroupings",		"Encoder_LEDRings",
				"Analog_LEDRings","AnalogMap_Notes",	"LEDGroupMode",	"LEDMapNotes2",	"LEDMapCCs2",	"ControlChannels", "ButtonToggle",	"EncoderFlipFlash",
				"ButtonPressColors", "CapFader_Notes",	"CapFader_CCs",	"CapFader_Values","CapFader_OutMode", "RGBStrip_Colors","FSRStateStreaming", "<untitled1>",
				"FSRStream(Response)","CapButtonMap",	"AnalogOutputFlags",	"7-Seg_States",	"LinkIndicators", "DisableCapFaderNotes", "AnalogCCStreamRate", "RGB_RedBlueSwap",
				"UNIMP(sysexThru)",	"UNIMP(AliasID)","Notes2CVMap", "EncoderDetents", "DS1LocalControl", "ElementConfig", "FirmwareVers", "AccelerSett"
)

#----------SysExDataSource: IndexedBy: SysEx Number------------------------------------------------- 
# if DataSource < 0x80, Fixed Number of Bytes.
# if DataSource >= 0x80, Refers to ControllerProperties table.
SysExDataSource		= (	2,			2,		0,		0,		0x80,		0,		0,			3,
				6,			3,		0x83,		0x81,		1,		1,		0,			1,
				0x87,			0x88,		0x83,		0x81,		0x87,		3,		1,			0x85,
				0,			0,		1,		0x82,		1,		0x88,		0x91,			0x87,
				1,			0x84,		0x86,		0x89,		0x89,		0x8A,		0x8B,			0x8C,
				0x88,			0x8E,		1,		0x88,		0x8D,		0x8D,		0x95,			0x87,
				0x83,			0x8E,		0x90,		0x89,		0x89,		5,		0x8F,			0,
				0x85,			0x92,		0x97,		0x92,		0x92,		0x93,		1,			0,
				0,			0x94,		0x8E,		0x96,		1,		1,		1,	0,
				0,			0,		32,		2,		0,		8,		0,	6)

#Data Source: 0-127 (constant_number), 	
#			0x80		0x81		0x82		0x83		0x84		0x85		0x86		0x87
DataSourceNames = (	"LEDBytes", 	"Buttons", 	"Buttons2", 	"Pots*2", 	"ExpansionPots*2","Banks", 	"Colors", 	"Encoders*2", 
#			0x88		0x89		0x8A		0x8B		0x8C		0x8D		0x8E		0x8F
			"2*EncoderBitBy","Max Notes/CCs","Banks*LEDBytes","Encoders", 	"Encoders*Banks","2*AnalogsBitBy","Analogs",	"ButtonColumns*2",
#			0x90		0x91		0x92		0x93		0x94		0x95		0x96		0x97
			"LEDGroups",	"EncoderSpeeds", "CapFaders",	"RGBStrips",	"CapButtons*2",	"LEDGroups/2",	"7-Segment Displays", "CapFaders*2")
#---------------------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------------
MessageNames		= ("NoteOff",	"NoteOn", "KeyPressure", "CC",	"ProgramChange",	"ChannelPressure",	"PitchBend")
MessageCodes		= (0x80,	0x90,		0xA0,	0xB0,		0xC0,			0xD0,		0xE0)

OptionNames		= ("SetChannel","CustomSysEx","SysExPresetFile")

ResponseNames		= ("Wait", "Cancel", "Nak", "Ack")
ResponseCodes		= (0x7c, 0x7d, 0x7e, 0x7f)

GlobalEditorSpecialCharacters = ['q', 'Q']
#---------------------------------------------------------------------------------------------------

#EditorOptionNames		= ("GlobalChannel", "CustomSysEx", "PresetFile")
#EditorOptionCodes		= (0,                	1,		2)

#---------------Special SysEx Messages-----------------------------------
SYSEX_LIVID_REQ_SETTINGS = [240, 0, 1, 97, 0, 7, 8, 247]
SYSEX_LIVID_REQ_SETTINGS_BRAIN = [240, 0, 1, 97, 1, 7, 8, 247]
#------------------------------------------------------------------------

#--------GlobalEditor Operators------------------------------------------
SX_MULT			=0x00
SX_SERIES		=0x01
SX_SINGLE		=0xFF
GLOBALEDITOR_ERR_DATAINPUT = 99
GLOBALEDITOR_EMPTY_DATAINPUT = 1023

DEFAULT_SEND_CHANNEL	=0x00
#------------------------------------------------------------------------

LividRequests           =[7,9]

CIN_BACKLIGHTS          =0x76 
CIN_LOGO                =0x77
LIVID_MIDI_REQ		=0x07

LIVID_VID		=0x04D8
OHM64_PID		=0xFE74
MINI_PID		=0xFD33
LIVID_MID		=0x00
LIVID_MID_1		=0x01
LIVID_MID_2		=0x61

LIVID_SYSEX_ACK		=0x7F
LIVID_SYSEX_REQUEST	=0x07
LIVID_SYSEX_HEADER 	= [0xF0, 0x00, 0x01, 0x61]

LIVID_SAVE_SETTINGS       =0x02
LIVID_SAVE_ALLBANKS       =25
LIVID_FACTORY_RESET	  =0x06
LIVID_CHANGE_BANK	  =26

SysEx_DefaultDataValues = (
				0x00,			0x00,		0x00,		0x00,		0x00,		0x00,			0x00,		0x00,
				0x00,			0x00,		0x00,		0x00,		0x00,		0x00,			0x00,		0x00,
				0x00,			0x00,		0x00,		0x00,		0x00,		0x00,			0x00,		0x00,
				0x00,			0x00,		0x00,		0x00,		0x00,		0x00,			0x01,		0x00,
				0x00,			0x00,		0x00,		0x7F,		0x7F,		0x00,			0x00,		0x00,
				0x00,			0x00,		0x00,		0x00,		0x00,		0x00,			0x00,		0x00,
				0x00,			0x00,		0x00,		0x00,		0x00,		0x00,			0x00,		0x00,
				0x00,			0x00,		0x0F
)	
