import time
import pypm
import os
import binascii

from pypmExtensions_h import *
from globalEditor_h import *

# Note:  sys.platform is now preferred over os.name for Operation Identification
#        if os.name == 'posix':				# Linux
#        elif os.name in ("nt", "dos", "ce"):           # Windows 
#        elif os.name == 'mac':         		# Macintosh
#        else:                               		# Undefined OS
#----------------------------------------

#-------------Clear MIDI Buffer-------------------
def ClearMidiInBuffer(MidiIn):
    while(MidiIn.Poll()):
        clearBuffer_rxmessage = GetOneFullMessage(MidiIn)
#-------------Clear MIDI Buffer---------------END-


#********ConnectToController(queryMessage, responseMessage)*******************
# Receives: 
#    message to send to a controller
#    message that valid controller will respond with
# Returns:  
#    Opened MIDI Input and MIDI Output of first found controller
#-----------------------------------------------------------------------------
def ConnectToController(query, response):	#!Note: This is not Functional or Finished
    devices = []                                #!Revision: Sends Query, Puts expected header of the Response in the AwaitingResponseQueue
    names = []                                  # - The AwaitingResponseQueue will check every Message
    numbers = []                                # - The AwaitingResponseQueue will empty itself after 500ms
    pairs = []                                  # - if Messages_AwaitingResponse > len(AwaitingResponseQueue): Acknowledge that a verification has failed
    IsAnInput = 0x01
    IsAnOutput = 0x02
    for loop in range(pypm.CountDevices()):
        number = loop
        device = pypm.GetDeviceInfo(loop)
        name = device[1]
        type = device[2] | device[3]<<1		# 0=invalid, 1=input, 2=output, 3+=invalid
        if type and type < 3:		# Valid Type
            if name in names:		# Midi Input/Output Couple
            #----------Couple Midi I/O---------------
                index = names.index(name)
                if type & IsAnInput:
                    pairs.append([number,index])
                else:
                    pairs.append([index,number])
                if types[index] == type:	# !Review: Remove this line and the next after testing
                    print "Warning: Multiple Ports of Same Name Detected (ConnectToController)"
            #----------Couple Midi I/O---------_end_-
            else:
                names.append(name)
                types.append(type)
        else:					# !Review: Remove this line and the next after testing
            print "Warning: Invalid Type Detected (ConnectToController)"
            
    # Now we have pairs, we need to query all MIDI Pairs, and try to find our controllers
    return True

def ConnectToMidiThruPort():
                       	#!Revision: Finds two ports of same name.  Sends Message, if same message received then it is a MIDI Through Port
    CONNECT_ERROR = 99
    num_devices = pypm.CountDevices()
    min = num_devices
    mon = num_devices
    for current_device_num in range(pypm.CountDevices()):
        interf,name,inp,outp,input_opened = pypm.GetDeviceInfo(current_device_num)
        name = name.lower()
        if 'midi through' in name:
            if inp:
                min = current_device_num
            else: #outp
                mon = current_device_num

    if min < num_devices and mon < num_devices:
        MidiThruIn, MidiThruOut = OpenMidiIO(min,mon,1)      # Revision: Add a Flag for NOT OPENED (or adjust for this flag)
        return MidiThruIn, MidiThruOut
    else:
        return CONNECT_ERROR, CONNECT_ERROR

#********GetMidiPairs()*******************
# Receives: 
#    Nothing 
#
# Returns:  A List 4 - Integers Detailing the Pairing of Midi Devices
#               1. Input # 
#		2. Output #
#               3. InputInUse?
#               4. OutputInUse?
#----------------------------------
def GetMidiPairs(MidiInputs, MidiOutputs, MidiIONumbers):
    Midi_Pairs = []
    for loop_inputs in range (len(MidiInputs)):
        for loop_outputs in range(len(MidiOutputs)):
           if MidiInputs[loop_inputs][1] == MidiOutputs[loop_outputs][1] and MidiInputs[loop_inputs][0] == MidiOutputs[loop_outputs][0]:
               single_pair = []
               single_pair = [MidiIONumbers[0][loop_inputs], MidiIONumbers[1][loop_outputs], MidiInputs[loop_inputs][4], MidiOutputs[loop_outputs][4] ]
               Midi_Pairs.append(single_pair)
    return Midi_Pairs

#********GetFullMidiList()*******************
# Receives: 
#    Nothing 
#
# Returns:  2 Lists (List of Midi Inputs/Outpus) 
#                   (List of 2 Lists [InputNumbers],[OutputNumbers])
#----------------------------------
def GetFullMidiList():			#!Revision: Add Midi Numbers to the Packet
    Midi_Table = []
    Input_Numbers = []
    Output_Numbers = []
    for loop in range(pypm.CountDevices()):
#        interf,name,inp,outp,opened = pypm.GetDeviceInfo(loop)
        current_device = pypm.GetDeviceInfo(loop)
        Midi_Table.append(current_device)
        if current_device[2]:
            Input_Numbers.append(loop)
        elif current_device[3]:
            Output_Numbers.append(loop)
    Midi_IO_Numbers = [Input_Numbers, Output_Numbers]	#!Revision: Output this table and get rid of GetMidiLists
    return Midi_Table#, Midi_IO_Numbers
#********GetFullMidiList()*******************


#********GetMidiLists()*******************
# Receives: 
#    Nothing 
#
# Returns:  List ( Driver Name, Port Name, Input?, Output?, Opened? )
#----------------------------------
def GetMidiLists():			#!Revision: Add Midi Numbers to the Packet
    Midi_Inputs_Table = []
    Midi_Outputs_Table = []
    Output_Numbers = []
    Input_Numbers = []
    for loop in range(pypm.CountDevices()):
#        interf,name,inp,outp,opened = pypm.GetDeviceInfo(loop)
        current_device = pypm.GetDeviceInfo(loop)
        if current_device[2]:
            Midi_Inputs_Table.append(pypm.GetDeviceInfo(loop))
            Input_Numbers.append(loop)
        elif current_device[3]:
            Midi_Outputs_Table.append(pypm.GetDeviceInfo(loop))
            Output_Numbers.append(loop)
    Midi_IO_Numbers = [Input_Numbers, Output_Numbers]
    return Midi_Inputs_Table, Midi_Outputs_Table, Midi_IO_Numbers
#********GetMidiLists()*******************

#********QueryLividDevice()*******************
# Receives: 
#    
#
# Returns:  Integer: Controller Number or 99 if no response
#----------------------------------
def QueryLividDevice(InputOutputList, timeout=300, latency=25):
    SysExVerifyEnabled = 1
    min = InputOutputList[0]
    mon = InputOutputList[1]
    input_is_opened = InputOutputList[2]
    output_is_opened = InputOutputList[3]
    cn = 99 # 99 -> Error Code: No Controller Found

    if input_is_opened or output_is_opened:
        print "It appears that device is already opened.", InputOutputList
    else:
        #---------- Open Midi Ports -----------
        midi_in = pypm.Input(min)
        midi_out = pypm.Output(mon, latency)
        #---------- Open Midi Ports -----------END

        #-----------Send SysEx Request And Get Response-----------------#
        output_message = SYSEX_LIVID_REQ_SETTINGS      	      #--- Prepare Output Message --------------------#
        expected_response = [0xF0, 0x00, 0x01, 0x61]          #--- Prepare Expected Response Message----------#
        sent_time = pypm.Time()
        rx_response = SendMessageReturnResponse(midi_in, midi_out, output_message, expected_response, timeout=300)
        #-----------Send SysEx Request And Get Response-------------END-#

        #---------------- Interpret Response -----------------------
        print "Rx Response: ",rx_response
        if rx_response == output_message:
            print "Midi Thru Port: ", InputOutputList
        elif rx_response[0:4] == LIVID_SYSEX_HEADER and rx_response[4] < len(ControllerNames):            
            response_time = pypm.Time()-sent_time
            cn = rx_response[4]
            print InputOutputList
            print "\nController Found [", ControllerNames[cn], "]: ", rx_response
            print "Delay: ",response_time, "ms"
        else:
            print InputOutputList
            print "Response Did Not Identify a Controller."
        #---------------- Interpret Response -----------------------
        del midi_in
        del midi_out	
    #raw_input('break')
    return cn      

#********GetRecognizedMidiControllers_complete()*******************
# Receives: 
#    Nothing 
#
# Returns:  All Recognized Controllers as a single list: [InputNumber, OutputNumber, ControllerNumber],[Input2Number,Output2Number,Controller2Number]  
#----------------------------------
def GetRecognizedMidiControllers_complete(timeout=300, latency=25):
    Recognized_Controllers = []
    cn = 99
    MidiPorts = GetFullMidiList()
    MidiInputs, MidiOutputs, MidiIONumbers = GetMidiLists()
    MidiPairs = GetMidiPairs(MidiInputs, MidiOutputs, MidiIONumbers)

    for loop in range (len(MidiPairs)):
        interface = MidiPorts[MidiPairs[loop][0]][0]
        name = MidiPorts[MidiPairs[loop][0]][1]
        name2 = name.split()
        #----Check For Valid Controllers-------------
        cn = QueryLividDevice(MidiPairs[loop])#, timeout, latency)
        #-----Add to table if is Valid Controller------
        if cn < len(ControllerNames):
            Recognized_Controllers.append([cn, MidiPairs[loop][0], MidiPairs[loop][1]])
#    print Recognized_Controllers
    return Recognized_Controllers
#--------------------------------------------_____!Revision: SearchForControllerInputs and SearchForControllerOutputs____ Need to be revised or removed

#********GetRecognizedMidiControllers()*******************
# Receives: 
#    Nothing 
#
# Returns:  All Recognized Controllers as a single list: [InputNumber, OutputNumber, ControllerNumber],[Input2Number,Output2Number,Controller2Number]  
#----------------------------------
def GetRecognizedMidiControllers(MidiPorts, MidiPairs):
    Recognized_Controllers = []
    cn = 99
    for loop in range (len(MidiPairs)):
        interface = MidiPorts[MidiPairs[loop][0]][0]
        name = MidiPorts[MidiPairs[loop][0]][1]
        name2 = name.split()
        #----Check For Valid Controllers-------------
        #if os.name == 'posix':				# Linux
        cn = QueryLividDevice(MidiPairs[loop])
        #elif os.name in ("nt", "dos", "ce"):    	# Windows
        #    cn = QueryLividDevice(MidiPairs[loop])
        #elif os.name == 'mac':            		# Macintosh (untested)
        #    cn = QueryLividDevice(MidiPairs[loop])
        #else:
        #    print "Couldn't Find OS"
        #-----Add to table if is Valid Controller------
        if cn < len(ControllerNames):
            Recognized_Controllers.append([cn, MidiPairs[loop][0], MidiPairs[loop][1]])
    print Recognized_Controllers
    return Recognized_Controllers
#--------------------------------------------_____!Revision: SearchForControllerInputs and SearchForControllerOutputs____ Need to be revised or removed

#********OpenRecognizedMidiControllers()*******************
# Receives: 
#    Nothing 
#
# Returns:  All Recognized Controllers as a single list: [InputNumber, OutputNumber, ControllerNumber],[Input2Number,Output2Number,Controller2Number]  
#----------------------------------
def OpenRecognizedMidiControllers(RecognizedMidiControllers):
#    min=[]
#    mon=[]
#    cn=[]
    MidiIn = []
    MidiOut = []
    opened_midi_controllers = []
    for loop in range(len(RecognizedMidiControllers)):
#        printHeader("hardwareTester: Recognized Controller Found -"+ControllerNames[RecognizedMidiControllers[option_number][0]])
        cn_temp = RecognizedMidiControllers[loop][0]
        min_temp = RecognizedMidiControllers[loop][1]
        mon_temp = RecognizedMidiControllers[loop][2]

        interf,name,inp,outp,input_opened = pypm.GetDeviceInfo(min_temp)
        interf,name,inp,outp,output_opened = pypm.GetDeviceInfo(mon_temp)

        if not input_opened and not output_opened:
            MidiIn_temp, MidiOut_temp = OpenMidiIO(min_temp,mon_temp,1)      # Revision: Add a Flag for NOT OPENED (or adjust for this flag)
            MidiIn.append(MidiIn_temp)
            MidiOut.append(MidiOut_temp)
            opened_midi_controllers.append(loop)
    return MidiIn, MidiOut, opened_midi_controllers


#----------------------------------------
#********FinishSysEx()*******************
# Requires: Rest of SysEx to be InPoll
#
# Receives: 
#    MidiIn: Midi Input Object
#    rx_message: First 4-Bytes of a SysEx Message (as read by MidiIn.Read(1))
#
# Returns:  Received Message
# - Entire SysEx Message
#----------------------------------
def FinishSysEx(MidiIn, rx_message):            # We have first four Bytes, now we must read for the next four Bytes
    finished = 0
    while not finished:            # We have first four Bytes, now we must read Four Byte Blocks until the message is finished (receive 247).
        index = 0
        if MidiIn.Poll():
            try:
                data = MidiIn.Read(1)
            #except Exception:			#!Review: Is this the way to deal with a Buffer Error?
            except: # pmBufferOverflow:			#!Review: Is this the way to deal with a Buffer Error?  -9969
                print "Buffer Overflowed: May have lost a MIDI Message or two"
                finished = 1
                index = 0xFF
        else:                           # Review: Does PortMidi put messages in Poll before they are complete?
            print "Warning... SysEx Did not finish with ", MIDI_SYSEX_END
            finished = 1
            index = 0xFF        	# Exit the "finished?" loop... we are unexpectedly finished.

        while index < 4:
            rx_message.append(data[0][0][index])
            if data[0][0][index] == MIDI_SYSEX_END:
                index = 0xFF
                finished = 0xFF
            else:
                index+=1
    return rx_message
#----------------------------------------
#********FinishSysEx()*************END***
#----------------------------------


#----------------------------------------
#********GetOneFullMessage()***************
# Requires: MidiInPoll()  (Message is ready to be read)
#
# Receives: MidiIn
#
# Returns:  Received Message  
# - 3 or 4 Bytes if is a normal MIDI Message
# - Entire Message if it is a SysEx Message
#----------------------------------
def GetOneFullMessage(MidiIn):		#---Requires: MidiInPoll
    rx_message = []
    try:
        MidiData = MidiIn.Read(1)
    except:  #pmBufferOverflow or pmHostError: #Exception:			#!Revision: Get the proper Error Code for Buffer Overflow
        print "My Buffer Overfloweth!"
        finished = 1
        index = 0xFF
        return []
    rx_message = MidiData[0][0]
    #rx_time = MidiData[0][1]
    if len(rx_message):
        message_type = MidiData[0][0][0]
        if message_type == MIDI_SYSEX:              # read only 4 bytes at a time... Our SysEx are a minimum of 7 Bytes
            rx_message = FinishSysEx(MidiIn, rx_message)
    return rx_message
#----------------------------------------
#********GetOneFullMessage()******END*******
#----------------------------------

#********GetOneFullMessage_andTime()***************
# Requires: MidiInPoll()  (Message is ready to be read)
#
# Receives: MidiIn
#
# Returns:  Received Message  
# - 3 or 4 Bytes if is a normal MIDI Message
# - Entire Message if it is a SysEx Message
#----------------------------------
def GetOneFullMessage_withTime(MidiIn):		#---Requires: MidiInPoll
    rx_message = []
    try:
        MidiData = MidiIn.Read(1)
        rx_message = MidiData[0][0]
        rx_time = MidiData[0][1]
        if len(rx_message):
            message_type = MidiData[0][0][0]
            if message_type == MIDI_SYSEX:              # read only 4 bytes at a time... Our SysEx are a minimum of 7 Bytes
                rx_message = FinishSysEx(MidiIn, rx_message)
        return rx_message, rx_time
    except: # Exception:			#!Revision: Get the proper Error Code for Buffer Overflow
        print "My Buffer Overfloweth!"
        finished = 1
        index = 0xFF
        return []
#----------------------------------------
#********GetOneFullMessage_andTime()******END*******
#----------------------------------

#----------------------------------------
#********GetOneFullMessage_noSysEx()***************
# Requires: MidiInPoll()  (Message is ready to be read)
#
# Receives: MidiIn
#
# Returns:  Received Message  
# - 3 or 4 Bytes if is a normal MIDI Message
# - Entire Message if it is a SysEx Message
#----------------------------------
def GetOneFullMessage_ignoreSysEx(MidiIn):		#---Requires: MidiInPoll
    rx_message = []
    try:
        MidiData = MidiIn.Read(1)
    except: # Exception:			#!Revision: Get the proper Error Code for Buffer Overflow
        print "My Buffer Overfloweth!"
        finished = 1
        index = 0xFF
        return []
    return MidiData[0][0]
#----------------------------------------
#********GetOneFullMessage()******END*******
#----------------------------------


#----------------------------------
#********GetSysExAck***************
# Receives: MidiIn, ControllerNumber, Timeout(ms) (optional)
#
# Returns: TimedOut?  
# - 0: if the Ack is Received
# - 1: if the Operation TimedOut
#
# Default TimeOut is 1000ms
#----------------------------------
def GetSysExAck(MidiIn, cn, timeout=300):
    MINIMUM_ACK_LENGTH = 6
    ACK = [240, 0, 1, 97, cn, 127, 247]
    sent_time = pypm.Time()
    cntr = 1
    responded = 0
    timed_out = 0
    while not responded:
        while not MidiIn.Poll() and not timed_out:
            if (pypm.Time()-sent_time > timeout):
                timed_out = 0xff
        if not timed_out:
            index = 0
            rx_message = []
            rx_message = GetOneFullMessage(MidiIn)
            if len(rx_message) >= MINIMUM_ACK_LENGTH:
                if rx_message[0:6] == ACK[0:6]:
                    responded = 0xff
                    response_time = pypm.Time()-sent_time
                    print "Received Ack [", cntr, "]: ", rx_message
                    print "Delay: ",response_time, "ms"
                else:
                    print "rx_message:", rx_message[0:6]
                    print "ack:       ", ACK[0:6]
            #else: Other Type of MIDI Message 
        else:
            responded = 0xff
    return timed_out
#----------------------------------
#********GetSysExAck********END****
#----------------------------------

#----------------------------------
#********GetSysExResponse***************
# Receives: MidiIn, ControllerNumber, Timeout(ms) (optional)
#
# Returns: TimedOut?  
# - 0: if the Ack is Received
# - 1: if the Operation TimedOut
#
# Default TimeOut is 1000ms
#----------------------------------
def GetSysExResponse(MidiIn, expected_response, timeout=300):
    sent_time = pypm.Time()
    cntr = 1
    responded = False
    timed_out = False
    while not responded:
        while not MidiIn.Poll() and not timed_out:
            time.sleep(.002)
            if (pypm.Time()-sent_time > timeout):
                timed_out = True
        if not timed_out:
            index = 0
            rx_message = []
            rx_message = GetOneFullMessage(MidiIn)
            if rx_message[0:len(expected_response)] == expected_response:
                responded = True
                response_time = pypm.Time()-sent_time
                print "Received Expected Message [", cntr, "]: ", rx_message
                print "Delay: ",response_time, "ms"
                #timed_out = False
            #else:
            #    print "rx_message:", rx_message
            #    print "rx_message_qlify:", rx_message
            #    print "expected_response:       ", expected_response
        else:
            responded = True
            print "timed out: " + str(pypm.Time()-sent_time) + "ms"
    return timed_out
#----------------------------------
#********GetSysExResponse********END****
#----------------------------------

#----------------------------------
#********GetSysExResponse_wReturn***************
# Receives: MidiIn, ControllerNumber, Timeout(ms) (optional)
#
# Returns: TimedOut?  
# - 0: if the Ack is Received
# - 1: if the Operation TimedOut
#
# Default TimeOut is 1000ms
#----------------------------------
def GetSysExResponse_wReturn(MidiIn, expected_response, timeout=300, silent=False):
    sent_time = pypm.Time()
    cntr = 1
    responded = 0
    timed_out = 0
    rx_message = []
    while not responded:
        while not MidiIn.Poll() and not timed_out:
            time.sleep(.002)
            #time.sleep(.005) !error: needs not to spin wheels here: we need this sleep!
            if (pypm.Time()-sent_time > timeout):
                timed_out = 0xff
        if not timed_out:
            index = 0
            rx_message = []
            rx_message = GetOneFullMessage(MidiIn)
            if not silent:
                print rx_message
                print expected_response
            if rx_message[0:len(expected_response)] == expected_response:
                responded = 0xff
                response_time = pypm.Time()-sent_time
                timed_out = 0
            #else:
            #    print rx_message[0:len(expected_response)]
            #    print expected_response
        else:
            rx_message = []
            responded = 0xff
    return rx_message
#----------------------------------
#********GetSysExResponse_wReturn********END****
#----------------------------------

#----------------------------------
#********SendMessageWithResponse***************
# Receives: MidiIn, ControllerNumber, Timeout(ms) (optional)
#
# Returns: TimedOut?  
# - 0: if the Ack is Received
# - 1: if the Operation TimedOut
#
# Default TimeOut is 1000ms
#----------------------------------
def SendMessageWithResponse(MidiIn, MidiOut, output_message, expected_response=[], timeout=300):    
    try:
        MidiOut.WriteSysEx(pypm.Time(), output_message)
    except:
        print "Error Writing to Midi Out Port"
    print output_message

    timed_out = GetSysExResponse(MidiIn, expected_response, timeout)
    return timed_out
#----------------------------------
#********SendMessageWithResponse********END****
#----------------------------------

#----------------------------------
#********SendMessageReturnResponse***************
# Receives: MidiIn, ControllerNumber, Timeout(ms) (optional)
#
# Returns: Message Received if One is Received
# - Empty List if the operation Times Out
# Default TimeOut is 300ms
#----------------------------------
def SendMessageReturnResponse(MidiIn, MidiOut, output_message, expected_response=[], timeout=300, silent=False):
    try:
        MidiOut.WriteSysEx(pypm.Time(), output_message)
    except:
        print "Error Writing to Midi Out Port"
        return []
    response = GetSysExResponse_wReturn(MidiIn, expected_response, timeout, silent)
    return response
#----------------------------------
#********SendMessageReturnResponse********END****
#----------------------------------


#----------------------------------
#********GetSysExReq***************
# Receives: MidiIn, ControllerNumber, Timeout(ms) (optional)
#
# Returns: TimedOut?  
# - 0: if the Ack is Received
# - 1: if the Operation TimedOut
#
# Default TimeOut is 1000ms
#----------------------------------
def GetSysExReq(MidiIn, cn=0, timeout=300):  #!revision: Global Inquiry Reply
    LIVID_MINIMUM_REQ_LENGTH = 6                  #!revision: Should be in lividMidi_h.py
    LIVID_SYSEX_HEADER = [240, 0, 1, 97]          #!revision: Should be in lividMidi_h.py
    ACK = [240, 0, 1, 97, cn, 127, 247]           #!revision: Should be in lividMidi_h.py
    sent_time = pypm.Time()
    cntr = 1
    responded = 0
    timed_out = 0
    while not responded:
        while not MidiIn.Poll() and not timed_out:
            if (pypm.Time()-sent_time > timeout):
                timed_out = 0xff
        if not timed_out:
            index = 0
            rx_message = []
            rx_message = GetOneFullMessage(MidiIn)
            if len(rx_message) >= LIVID_MINIMUM_REQ_LENGTH:
                if rx_message[0:4] == LIVID_SYSEX_HEADER and rx_message[5] < len(SysExNames):
                    responded = 0xff
                    response_time = pypm.Time()-sent_time
                    print "\nReceived Response [", cntr, "]: ", rx_message
                    print "Delay: ",response_time, "ms"
        else:
            rx_message = []
            responded = 0xff
    return rx_message, timed_out  # Note: when valid Request received...rx_message[4] = LividControllerNumber
#----------------------------------
#********GetSysExReq********END****
#----------------------------------

def OpenMidiIO (midiInputNumber, midiOutputNumber, latency=LATENCY_100ms):
    return (pypm.Input(midiInputNumber), pypm.Output(midiOutputNumber, latency))

def GetABYTE(message, floor=0, ceiling=128, options_enabled=0):          # warning: limited to range(256)
    n = " "
    under_limit = 0
    while 1:
        while not n.isdigit() and not n in GlobalEditorSpecialCharacters:
            n = raw_input(message)
        if n.isdigit():
            n = int(n)
            if n in range(floor,ceiling):
                return n
            else:
                n = ' '
        elif options_enabled:
            return "error"            # Review: Should do something else here..

def GetABYTEorQuit(message):          # warning: limited to range(256)
        n = " "
        while not n.isdigit() and not n in GlobalEditorSpecialCharacters:
            n = raw_input(message)
        if n.isdigit():
            return int(n) & 0x7F
        else:
            return "error"


def PrintDevices(InOrOut):
    for loop in range(pypm.CountDevices()):
        interf,name,inp,outp,opened = pypm.GetDeviceInfo(loop)
        if ((InOrOut == INPUT) & (inp == 1) |
            (InOrOut == OUTPUT) & (outp ==1)):
            print loop, name," ",
            if (inp == 1): print "(input) ",
            else: print "(output) ",
            if (opened == 1): print "(opened)"
            else: print "(unopened)"
    print

def PrintAllMIDIDevices():
    for loop in range(pypm.CountDevices()):
        interf,name,inp,outp,opened = pypm.GetDeviceInfo(loop)
        print loop, name," ",
        if (inp == 1): print "  (input) ",
        else: print "   (output) ",
        if (opened == 1): print "       (opened)"
        else: print "   (unopened)"
    print

def printHeader(message):
        print LINE_BREAK
        print LINE_BREAK
        print message
        print LINE_BREAK
        print LINE_BREAK

def clearScreen():
        if os.name == "posix":        # Unix/Linux/MacOS/BSD/etc
                os.system('clear')
        elif os.name in ("nt", "dos", "ce"):        # DOS/Windows
                os.system('CLS')
        else:
        # Fallback for other operating systems.
                print '\n' * 50


#******** convertMessageToDecString()*******************
# Receives: A List of Integers 
# Returns:  A Hexidecimal String Representing the entire Message
# - Returned String is formatted for direct file writing
# - Returned String does Not contain the ordinary '0x' pre-text for these numbers
#----------------------------------
def convertMessageToDecString(list_of_integers):
    dec_response = []
    dec_response_string = str(list_of_integers)
    dec_response_string = dec_response_string.replace('[','').replace(']','').replace(',', '')
    return dec_response_string


#******** convertMessageToHexString()*******************
# Receives: A List of Integers 
# Returns:  A Hexidecimal String Representing the entire Message
# - Returned String is formatted for direct file writing
# - Returned String does Not contain the ordinary '0x' pre-text for these numbers
#----------------------------------
def convertMessageToHexString(list_of_integers):
    hex_response = []
    hex_response_string = ''
    for response_index in range(len(list_of_integers)):
        try:
            hex_response.append( hex(list_of_integers[response_index]) )
        except:
            print "Error! Not a hex digit"
            return []

    for response_index in range(len(hex_response)):
        hex_response_string += hex_response[response_index].replace('0x', '').upper().zfill(2) + ' ' 
    return hex_response_string

#******** convertMessageToBinaryString()*******************
# Receives: A List of Integers 
# Returns:  A Hexidecimal String Representing the entire Message
# - Returned String is formatted for direct file writing
# - Returned String does Not contain the ordinary '0x' pre-text for these numbers
#----------------------------------
def convertMessageToBinaryString(list_of_integers):
    bin_response_string = ''
    for response_index in range(len(list_of_integers)):
        bin_response_string += '%c' % list_of_integers[response_index]
    return bin_response_string

    #-----------binascii method------------------------
    #for response_index in range(len(list_of_integers)):
    #    try:
    #        hex_response.append( hex(list_of_integers[response_index]) )
    #    except:
    #        print "Error! Not a hex digit"
    #        return []
    #bin_response_string = binascii.a2b_hex(bin_response_string)
    #return bin_response_string
    #-----------binascii method--------------------end-
