import readline, glob
import os

def complete(text, state):
    return (glob.glob(text+'*')+[None])[state]

#------Open Input and Output Files----#
def EnableTabAutoComplete():
    if os.name == "posix":
        readline.set_completer_delims(' \t\n;')
    readline.parse_and_bind("tab: complete")
    readline.set_completer(complete)

#filename = raw_input ("Data File:")
#print "Opening the File..."
#readFile = open(filename, 'r')
#------Open Input and Output Files----#


